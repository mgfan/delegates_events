﻿using System;
using System.Threading;

namespace Timer
{
	public class Timer
	{
		public event Action<string, int> Started;
		public event Action<string, int> Tick;
		public event Action<string> Stopped;

		private string name;
		public string Name
		{
			get => name;
			set
			{
				if (value == "" || value == null)
				{
					throw new System.ArgumentException("Имя не может быть пустым", "Name");
				}
				else
				{
					name = value;
				}
			}
		}

		private int ticks;
		public int Ticks
		{
			get => ticks;
			set
			{
				if (value > 0)
				{
					ticks = value;
				}
				else
				{
					throw new System.ArgumentException("Количество тиков должно быть больше 0", "Ticks");

				}
			}
		}

		public Timer(string name, int ticks)
		{
			this.Name = name;
			this.Ticks = ticks;
		}

		public void Start()
		{
			Started?.Invoke(Name, Ticks);
			for (int i = Ticks-1; i > 0; i--)
			{
				Tick?.Invoke(Name, i);
				Thread.Sleep(10);
			}
			Stopped?.Invoke(Name);

		}
	}
}