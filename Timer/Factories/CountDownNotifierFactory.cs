﻿using System;
using Timer.Interfaces;
using Timer.Implementation;

namespace Timer.Factories
{
	public class CountDownNotifierFactory
	{
		public CountDownNotifier CreateNotifierForTimer(Timer timer)
		{
			return new CountDownNotifier(timer);
		}
	}
}
