﻿using System;
using System.Threading;
using Timer.Interfaces;

namespace Timer.Implementation
{
	public class CountDownNotifier : ICountDownNotifier
	{
		private readonly Timer Time;

		public CountDownNotifier(Timer time)
        {
			if (time == null)
				throw new System.ArgumentNullException(nameof(time), "В качестве экземпляра Timer не может передоваться null");
			Time = time;
        }

		public void Init(Action<string, int> startDelegate, Action<string> stopDelegate, Action<string, int> tickDelegate)
		{
			if (startDelegate != null)
				Time.Started += startDelegate;
			if (tickDelegate != null)
				Time.Tick += tickDelegate; // Назначаем передаваемые методы в качестве соответствующих событий
			if (stopDelegate != null)
				Time.Stopped += stopDelegate;
		}

		public void Run()
		{
			Time.Start();	
		}
	}
}